# HOT - ADS 

# Group Members

| Student Name   | Email                |
| :------------- | -------------------: |
| David Roberto  | up202003057@fe.up.pt |           
| Diogo Xambre   | up202003081@fe.up.pt |
| Nuno Ferreira  | up202000059@fe.up.pt |
| Tiago Maia     | up202001526@fe.up.pt |


# Documentation

# Introduction

This project was developed within the scope of the architecture and software design course, with the purpose of allowing students to explore the architecture and design patterns present in the world of software engineering and show its importance in the creation or updating of new types of software.


# Goals of the Project

The main goals of the House of Things (HoT) project is to develop a software system to monitor, control and manage home automation devices and activities. The system that was implemented should support devices from all different kinds (sensors, actuators, etc.), should be able to add new types of devices easily to the system and be able to trigger events according to premeditated events/triggers that will activate certain actuators and sensors.

The system should also be able to support the discoverability of new devices, should have a clear interface for the user to navigate and integrate a notifications system such as SMS, WhatsApp, Slack, Discord, etc. 


# Domain Model

In our domain model, it's possible to identify a class device. This class has two subclasses, one being the Actuator and the other being the Sensor. The actuator, as well as the sensor, do have subclasses that implement different types of actuators and sensors respectively. 

It is also possible to check that each device has a value, and there are several types of values. The value is also part of the condition, meaning that for a condition to be met, the value of the sensor needs to be compared to the value of the condition defined.

Since there are different types of values, there must be different subclasses that extend from Condition. This means that these conditions need to have a reference to the value that they use.

This information about the system, all the devices, and conditions are stored in the HomeManagerModel. Several controllers have a reference to the HomeManagerModel, to send the information from the model to the view.

Each device is also part of the Event class, where is stored all the conditions and references of the devices that subscribed or publish those events. 


![Domain Model](https://gitlab.com/Tiago_Maia/hot-ads/-/raw/master/img/domain_model.png "Domain Model")





# Patterns used in the HOT Project

| Pattern Name          | Link                  |
| :-------------------- | ------------------:   |
| Publisher Subscriber  | [Click Here](#publisher-subscriber)|  
| Model View Controller | [Click Here](#mvc)    | 
| Singleton             | [Click Here](#singleton)|
| Strategy              | [Click Here](#strategy)|
| Builder               | [Click Here](#builder)|  
| Microkernel           | [Click Here](#microkernel)|
            



## 1. Publisher Subscriber Pattern <a name="publisher-subscriber"></a>

### &nbsp;&nbsp;1.1. Design Context and the Problem

The problem that we were facing was how to create a communication channel between the sensors and the actuators. This communication channel was going to be needed when the sensor triggered an event. Only the actuators that were waiting for that trigger would need to be advised, while the others shouldn't be.

The creation of this channel of communication was very challenging because it's the main element of the project itself and the way the different sensors send notifications to the actuators. We needed a system/pattern that was able to fill in the requirements that would solve this issue in a way that doesn't couple the components too tightly, making them lose their independence. We would also need to find a way that actuators would not depend on sensors to be able to work.

###  &nbsp;&nbsp;1.2. The Pattern
In order to solve this problem, we decided to implement the Publisher Subscriber pattern into our code.

The Publisher-Subscriber pattern defines a propagation infrastructure that allows the publishers to disseminate events that convey information that may be of interest to others. These others are the subscribers, that may be or not interested in the information published by the publisher. 

In our system, the publishers are the sensors and are charged with publishing the events. The sensor only publishes an event if a certain condition assigned to it is disputed. On the other hand, the actuator only subscribes to an event that has been raised and will operate according to it.

For example, let's considered an event (condition) of a motion sensor detecting movement. If the motion sensor activates that event, if there is an actuator subscribed to it,  it will receive a message and act according to it.  

### &nbsp;&nbsp;1.3. Implementation

#### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1.3.1. (Subscriber) Class LightActuator [Link to Code](https://gitlab.com/Tiago_Maia/hot-ads/-/blob/master/src/Devices/Actuators/LightActuator.java) or Class SlackActuator [Link to Code](https://gitlab.com/Tiago_Maia/hot-ads/-/blob/master/src/Devices/Actuators/SlackActuator.java)

Classes that were created in order to add new actuators to the infrastructure and manage the existing actuators. 

Inside each class, there is a OnMessage method that is activated once a publisher publishes a message. All actuators that receive the message from the events subscribed, need to implement this method to know how to act. 


#### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1.3.2. (Publisher) Class Sensor  [Link to Code](https://gitlab.com/Tiago_Maia/hot-ads/-/blob/master/src/Devices/Sensor.java)

Class that was created in order to add new sensors to the infrastructure and manage the existing sensors. 

Inside the class, there is a check condition method that is called when the sensor changes value. This way, the sensor verifies each condition that it has and if any is triggered, calls the method execute, publishing the event.


#### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1.3.3. Class Message [Link to Code](https://gitlab.com/Tiago_Maia/hot-ads/-/blob/master/src/Communication/Message.java)

Class that was created in order to create a message that could be published by the sensor (Publisher). This message has a reference to condition the condition triggered, where is stored the behavior of the actuator.


#### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1.3.4. Class Event [Link to Code](https://gitlab.com/Tiago_Maia/hot-ads/-/blob/master/src/Communication/Event.java)

Class created in order to be called by the Publisher and the subscriber to execute operations.In this class that we have created an HashMap to store our subscriber's references.

In this class, there are also two main operations expected in an Event, which are the operation Publish and the operation Subscribe.
    
For the subscribe method called by the subscriber (actuators), we pass a condition, and the subscriber that needs to be listening.  

For the publishing method called by the publisher (sensors), we pass a message. This way, when the subscriber gets notified by an event being raised will also receive an object of type message that can inform all other details.



### &nbsp;&nbsp;1.4. Consequences

#### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1.4.1 Benefits
This pattern facilitates the process of communication when adding new components in the future. In this case, the exchange of events occurs without the participants knowing anything about each other. This creates an abstraction on event handling that makes any subscriber receives messages from any publisher.
Another benefit that comes with using this pattern is that the notifications sent by the sensors only go to the interested actuators that have "subscribed" to that information. 


#### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1.4.2 Liabilities
In our instantiation of the Publisher-Subbsciber pattern the main liability, it’s the fact of the communication only being one way, in this case, sensor (publisher) to an actuator (subscriber). For example, if we wanted a sensor communicating with another sensor it would be needed additional changes.


## 2. Model View Controller Pattern <a name="mvc"></a>

### &nbsp;&nbsp;2.1. Design Context and the Problem

The problem that we were facing was how to create a user interface that needed to be easy to change and would not degrade the application quality. 

Another problem was that, in the initial stage of the project, we decided to use JavaFX in our interface which meant we had to use fxml files for the interface only. For this interface to communicate with our code, it would be needed some kind of controller.


### &nbsp;&nbsp;2.2. The Pattern
In order to solve these problems, we decided to implement the Model View Controller pattern into our code. This pattern divides an interactive application into three decoupled parts: processing, input, and output. These three parts are called model, view, and controller. 

The model is what manages the data, logic, and rules of the application. In our system, there is a class HomeManagerModel that represents the model of the MVC.

The view is used to present the user data from the model. In our case, we have several views, for each function of the system. The “AddDevice.fxml”, the “AddRoom.fxml”, the “Condition.fxml” and “Home.fxml” are all views.

The controller is what accepts inputs from the user and converts them to commands for the Model and the View. In our case, each view has its controller class,  the “AddDeviceController”, the “AddRoomController”, the “ConditionController” and “HomeController” are all controllers of their respective view.


### &nbsp;&nbsp;2.3. Implementation

#### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2.3.1 Class HomeManagerModel [Link to Code](https://gitlab.com/Tiago_Maia/hot-ads/-/blob/master/src/HomeManager/HomeManagerModel.java)

This is the class that manages all of the objects and data that is created and edited in the system. It can add new Sensors, Actuators, Rooms and Conditions. 

#### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2.3.2 Controllers

These are the classes that control the flow of information inserted from the user and convert that information into commands for the Model and View. All of the information that comes from the interface passes through the respective controller and then it goes to the Model to be processed.

#### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2.3.2.1 Class AddDeviceController [Link to Code](https://gitlab.com/Tiago_Maia/hot-ads/-/blob/master/src/HomeManager/Controller/AddDeviceController.java)

This controller class was created in order to control the view that shows the interface of adding new devices to the system. This class is responsible for converting the information inserted by the user into the commands of the model of adding new devices. 

#### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2.3.2.2 Class AddRoomController [Link to Code](https://gitlab.com/Tiago_Maia/hot-ads/-/blob/master/src/HomeManager/Controller/AddRoomController.java)

This controller class was created in order to control the view that shows the interface of adding new rooms to the system. This class is responsible for converting the information inserted by the user into the commands of the model of adding new rooms. 

#### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2.3.2.3 Class ConditionController [Link to Code](https://gitlab.com/Tiago_Maia/hot-ads/-/blob/master/src/HomeManager/Controller/ConditionController.java)

This controller class was created in order to control the view that shows the interface of adding new conditions to the system and associate conditions with sensors and actuators. This class is responsible for converting the information inserted by the user into the commands of the model of adding a new condition, associating a sensor to a certain condition, and also associating an actuator to a certain condition. 

#### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2.3.2.4 Class HomeController [Link to Code](https://gitlab.com/Tiago_Maia/hot-ads/-/blob/master/src/HomeManager/Controller/HomeController.java)

This controller class was created in order to control the view that shows our system information. This class is responsible for updating the view when a new device is added to the system. This class is responsible for converting the information inserted by the user into the commands of the model of changing device state and value.

#### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2.3.3 Views

These files are used to present the data and information from our system to the user.

#### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2.3.3.AddDevice.fxml [Link to Code]( https://gitlab.com/Tiago_Maia/hot-ads/-/blob/master/src/Resources/AddDevice.fxml)

This file is used to present the data from adding a new device.

#### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2.3.3.2 AddRoom.fxml [Link to Code]( https://gitlab.com/Tiago_Maia/hot-ads/-/blob/master/src/Resources/AddRoom.fxml )

This file is used to present the data from adding a new room.

#### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2.3.3.3 Class Condition.fxml  [Link to Code]( https://gitlab.com/Tiago_Maia/hot-ads/-/blob/master/src/Resources/Condition.fxml   )

This file is used to present the data the conditions.

#### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2.3.3.4 Class Home.fxml  [Link to Code]( https://gitlab.com/Tiago_Maia/hot-ads/-/blob/master/src/Resources/Home.fxml)

This file is used to present the data from the devices and rooms in our system.

### &nbsp;&nbsp;4. Consequences

#### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4.1 Benefits
The main benefit that comes with us using the MVC pattern is the ability for us to divide our system into multiple views and, in that way, simplify the user interface for an easier understanding of the contents that the system provides. This pattern will facilitate our work when creating new views.

#### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4.2 Liabilities
One liability that comes with using the MVC pattern in our project is the increased complexity that comes with using multiple views and the way to coordinate the information between them and the controllers.

Another liability was that we needed to ensure that we only had one HomeManagerModel in the system, otherwise different controllers could be communicating with different instances of the same model. This was a problem that is described more concretely in the section below.


## 3. Singleton Pattern <a name="singleton"></a>

### &nbsp;&nbsp;3.1. Design Context and the Problem

The problem we were facing was that we needed to create a single endpoint where the same data is accessed and manipulated by all classes. In our system, we had the same problem twice.
 
One of these problems was that we needed to have the same instance of the class HomeManagerModel accessible by all classes. This class has all the rooms, conditions, and devices that when is manipulated maintains the data integrity. Also, the class Event responsible for the communication between devices needed to have a single instance in order to have only one channel responsible to exchange messages.

To achieve this purpose, we needed a pattern that was able to have a single and only instance of an object that can be accessed by any object in the system.


### &nbsp;&nbsp;3.2. The Pattern

In order to solve this issue, we decided to implement the Singleton pattern into our code.

The Singleton pattern ensures that a class has only one instance and be accessible by all classes.

Concretely in our system, the class HomeManagerModel has only one instance that is accessed by three classes such as AddRoomController, HomeController, AddDeviceController, and ConditionController. This ensures that these controllers manipulate the same data provided by the HomeMangarModel. 

The class Event also has only one instance to provide the same channel to ensure the integrity of the communication.

For example, if we create devices on the class AddDeviceController and added to the model this same device can be accessed by all classes.


### &nbsp;&nbsp;3.3. Implementation

#### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3.3.1. Class HomeManagerModel [Link to Code](https://gitlab.com/Tiago_Maia/hot-ads/-/blob/master/src/HomeManager/HomeManagerModel.java)

Class created that has data structures that store the main objects of the system such as sensors, actuators, rooms, and conditions. A single instance of this object assure that the same data structures are manipulated.

#### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3.3.2. Class Event [Link to Code](https://gitlab.com/Tiago_Maia/hot-ads/-/blob/master/src/Communication/Event.java)

Class created that has a data structure, in this case, an HashMap that stores the condition and the device for each key and value. A unique instance of the object Event assures that, in the system, there is a single HashMap responsible for having a single channel of communication.

### &nbsp;&nbsp;3.4. Consequences

#### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3.4.1. Benefits
The benefits of the pattern Singleton are that allows having coherence on the data and integrity in the communication of the system. 

#### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3.4.2. Liabilities
Eventually, this pattern can present some liabilities such as the need to have more than one channel of communication. For example, if there is a need to have another channel for a specific sensor and actuator. Also if there is a need to have a different data model, that in the case of this system, there is no need to have different data models.

## 4. Strategy Pattern <a name="strategy"></a>

![Strategy](https://gitlab.com/Tiago_Maia/hot-ads/-/raw/master/img/Strategy.png "Strategy")

### &nbsp;&nbsp;4.1. Design Context and the Problem 

Whilst developing the system, we came across a problem that we need to separate parts of our program that vary according to what the system needs. These code parts also need to vary independently from the class that uses them. 
Concretely, the sensor needs to compare its condition with its current value. This comparison algorithm varies according to the type of sensor (the type of sensor value), so there is a need for decoupling this comparison from the condition so that it can evolve independently. So, we need that a certain sensor, like a movement sensor, to compare its movement value (boolean) with a concrete movement condition.

### &nbsp;&nbsp;4.2. The Pattern
To address this problem, we decided to implement the strategy pattern.
According to the concept of the strategy pattern, it encapsulates each algorithm and makes it commutable for concrete algorithm implementation of it.
In our system, the class Sensor has an abstract condition that has also an abstract compare method. The subclasses of this Condition Class herd this abstract method and implement it according to its logic. So a concrete sensor with a concrete condition has a compare condition that is according to the logic of the sensor.
For example, if we have a concrete sensor, this can be a movement sensor with a movement condition associated. When the sensor value is updated, there is a call to the compare method that is responsible to compare the movement value with the threshold of the condition.

### &nbsp;&nbsp;4.3. Implementation

#### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4.3.1 Abstract Class Sensor [Link to Code](https://gitlab.com/Tiago_Maia/hot-ads/-/blob/master/src/Devices/Sensors/Sensor.java)

Abstract Class created that has an abstract condition. When a specific sensor class is instantiated it can have a specific condition according to the sensor value.

#### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4.3.2 Abstract Class Condition [Link to Code](https://gitlab.com/Tiago_Maia/hot-ads/-/blob/master/src/Conditions/Condition.java)

An abstract class that has an abstract method called compare. With this abstract method, all of the subclasses can have their implementation and evolve independently.

#### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4.3.2.1 Class MovementCondition [Link to Code](https://gitlab.com/Tiago_Maia/hot-ads/-/blob/master/src/Conditions/ConditionType/MovementCondition.java)

A class that has a compare condition that has the logic for comparing values of the type Movement values with the threshold, and also with specific operators. 


#### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4.3.2.2 Class ThermometerCondition [Link to Code](https://gitlab.com/Tiago_Maia/hot-ads/-/blob/master/src/Conditions/ConditionType/ThermometerCondition.java)

A class that has a compare condition that has the logic for comparing values of the type Thermometer values with the threshold, and also with specific operators.

### &nbsp;&nbsp;4.4. Consequences

#### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4.4.1 Benefits

The benefit that stands out of the usage of this pattern is the possibility to have an abstraction on the sensor and leaving the value comparison to being defined in the subclasses of condition. This way we can create new types of conditions independently of the class Sensor.

#### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4.4.2 Liabilities

One liability that this pattern has is the possibility to have a similar code in the algorithms. More concretely in our system, the compare method in the class MovementCondition has some sort of similar code with the class ThermometerCondition. For example, both classes have the operator “!=” and “==”, when a sensor value is compared with the threshold of the condition.


## 5. Builder Pattern <a name="builder"></a>

![Builder](https://gitlab.com/Tiago_Maia/hot-ads/-/raw/master/img/Builder.png "Builder")

### &nbsp;&nbsp;5.1. Design Context and the Problem 

The problem that we came across was when we created a new device, how do we know which initial value would the device have. This problem happens because each device has different types of values that can have, for example, a light actuator can have an RGB value while a movement sensor needs a Boolean (detecting or not detecting). 

This way we needed the AddDeviceController to be able to construct a complex object (value) for the device while being shielded from the details related to that object(value).


### &nbsp;&nbsp;5.2. The Pattern

In order to solve this issue, we decided to implement the Builder pattern into our code. 

The builder pattern allows a client object to construct a complex object by specifying only its type and content. This pattern intends to separate the construction from the representation. By doing so, the same construction process can create different representations. 


### &nbsp;&nbsp;5.3. Implementation

#### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5.3.1 Class AddDeviceControler [Link to Code](https://gitlab.com/Tiago_Maia/hot-ads/-/blob/master/src/HomeManager/Controller/AddDeviceController.java)

A Class that constructs the new complex object (value) using the DeviceController interface.

#### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5.3.1 Interface DevicesControler [Link to Code](https://gitlab.com/Tiago_Maia/hot-ads/-/blob/master/src/Devices/DevicesController.java)

An interface where is specified a method for getting (creating) new values.

#### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5.3.1 Class SlackActuatorControler [Link to Code](https://gitlab.com/Tiago_Maia/hot-ads/-/blob/master/src/Devices/Actuators/SlackActuatorControler.java)

A class where is created a new value, in this case, a slackMessageValue, by getting the values from the corresponded view.

#### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5.3.1 Class LightActuatorControler [Link to Code](https://gitlab.com/Tiago_Maia/hot-ads/-/blob/master/src/Devices/Actuators/LightActuatorControler.java)

A class where is created a new value, in this case, a LightValue, by getting the values from the corresponded view.

#### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5.3.1 Class MovementControler [Link to Code](https://gitlab.com/Tiago_Maia/hot-ads/-/blob/master/src/Conditions/Condition.java)

A class where is created a new value, in this case, a MovementValue, by getting the values from the corresponded view.

#### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5.3.1 Class ThermometerSensorControler [Link to Code](https://gitlab.com/Tiago_Maia/hot-ads/-/blob/master/src/Conditions/Condition.java)

A class where is created a new value, in this case, a ThermometerValue, by getting the values from the corresponded view.

### &nbsp;&nbsp;5.4. Consequences

#### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5.4.1 Benefits

By using this pattern we were able to create any complex object (Value) in the class AddDeviceController, while shielding the details of Value from the AddDeviceController.  This way, the construction process can be used to create different representations because the logic of this process is isolated from the actual step of creating the value in the class AddDeviceController.

#### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5.4.2 Liabilities

There are almost no disadvantages when using this pattern. The only disadvantage that we were able to identify is the need of creating a new class for each new value, which creates more complexity. 


## 6. Microkernel Pattern <a name="microkernel"></a>

### &nbsp;&nbsp;6.1. Design Context and the Problem 

In the project, we faced a new issue regarding the capability to integrate new entities into the system and not knowing in advance what entities will get integrated. 
In this context, we need to make the core system capable to function with new kinds of devices. If there is a need to add a new sensor or a new actuator unknown by the system we need to have the necessary infrastructure.

### &nbsp;&nbsp;6.2. The Pattern

To tackle this problem, we decided to implement the microkernel pattern.

This pattern is used when the system must be able to adapt to changes and become plug-in capable, in order to extend functionalities to the system.

Regarding our system, with the usage of this pattern, becomes capable to add new kinds of devices and conditions. For that, we only need to create a new class of the desired device and extend the class Actuator or the class Sensor. Them we need to add the class name to the corresponding enum. If this new device supports a new kind of value, we proceed to create a new value type and extend to the abstract class Value and create a new class of condition (supporting that value) that extends to the abstract class Condition. Also, there is a need to create a view and a controller to support both the device extension and also the condition extension.

For example if there is a need to add a new device that can be a fire detector sensor. To add this new kind of device we need to create a class for this sensor and extends the Class Sensor. Also we need to add to the enum SensorTypes the new class name. This new sensor has a new type of values, so there is a need to create a new class that extends the Value class. Finally, there is a need to create a view and a controller for this new sensor.

### &nbsp;&nbsp;6.3. Consequences

#### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;6.3.1 Benefits

The main benefit that comes with using the microkernel pattern instantiation is that it allows us to be more agile to respond to the creation of new devices and conditions, in that way creating fairly robust system that requires few changes over time. 

#### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;6.3.2 Liabilities

The main liability that the microkernel pattern brings, is the fact that it's design and implementation of the is far more complex than that of a monolithic system.


# Test Cases
| Test Case          | Test Case Description | Test Steps |
| :-------------------- | :------------------ | :------------------|
| #1 | Add new Room | 1) In the "Add Room", select the type of room; <br/> 2) Name the room; <br/> 3) Click add room; <br/> 4) In the "Home", check in the select box the room that you add;  
| #2 | Add New Device | 1) In the Add Device, select the type of device you want to enter(sensor or actuator); <br/> 2) Select the type of the device you have picked (different between the two); <br/> 3) Input the start values of the device; <br/> 4) Check the Home, to see the added device;
| #3 | Add new Condition |  1) In the Add Condition, create a new condition;<br/> 2) Select the type of input the condition will get and the value that will trigger it; <br/> 3) Check the list below to see the added condition;
| #4 | Link Actuator to a Condition |  1) In the Add Condition, select a condition that you want to add a actuator to;<br/> 2) Below, select the actuator you want to add to the condition;<br/> 3) Select the values that will change once the condition was triggered;<br/> 4) Click on the condition to see the associated actuators and their triggered values;
| #5 | Link Sensor to a Condition |  1) In the Add Condition, select a condition that you want to add a sensor to;<br/> 2) Below, select the sensor you want to add to the condition;<br/> 4) Click on the condition to see the associated sensors and their triggered values;
| #6 | Update Sensor Value |  1) In the Home, select the sensor you want to change the value; <br/> 2) Change the value of the sensor to the wanted value;
| #7 | Turn ON/OFF Device |  1) In the Home, select the device you want to turn on or off;<br/> 2) In the column ON/OFF, click the value; <br/>3) Choose the wanted value from the dropbox;<br/> 4) If on, the light will be green, if off, the light will be red;


