package Devices;

import House.Room;
import Values.Value;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

public abstract class Device {
    private Room room;
    private DeviceState deviceState;
    private static int number = 0;
    private final int id;
    private String deviceName;
    private Circle circle;
    private Value value;

    public Device(Room room, String deviceName, DeviceState deviceState, Value value) {
        this.room = room;
        this.id = number;
        this.deviceName = deviceName;
        this.deviceState = deviceState;
        this.circle = new Circle(5);
        this.circle.setFill(Color.GREEN);
        number++;
        this.value = value;
    }

    private void setCircleColor(){
        if(getDeviceState() == DeviceState.OFFLINE){
            this.circle.setFill(Color.RED);
        }else if(getDeviceState() == DeviceState.ONLINE){
            this.circle.setFill(Color.GREEN);
        }
    }

    public DeviceState getDeviceState(){
        return deviceState;
    }

    public Room getRoom() {
        return room;
    }

    public int getId() {
        return id;
    }

    public String getDeviceName(){
        return deviceName;
    }

    public void setDeviceState(DeviceState deviceState) {
        this.deviceState = deviceState;
        setCircleColor();
    }

    public Circle getCircle(){
        return this.circle;
    }

    public Value getValue() {
        return value;
    }

    public void setValue(Value value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return  "" + room + "\n" + "\nDeviceState: " + deviceState + "\nId: " + id + "\nDeviceName: " + deviceName + " " + value;
    }
}
