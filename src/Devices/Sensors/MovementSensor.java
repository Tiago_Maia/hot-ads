package Devices.Sensors;

import Devices.DeviceState;
import Devices.Sensors.Sensor;
import House.Room;
import Values.MovementValue;
import Values.Value;

public class MovementSensor extends Sensor {

    public MovementSensor(Room room, String deviceName, DeviceState deviceState, Value value) {
        super(room, deviceName, deviceState, value);
    }

    @Override
    public String getValueString() {
        MovementValue mv = (MovementValue)getValue();
        if(mv.getSensorValue() == true){
            return "true";
        }else{
            return "false";
        }
    }

    public void setValue(MovementValue value) {
        checkCondition(value);
        super.setValue(value);
    }

    @Override
    public String toString() {
        MovementValue mv = (MovementValue)getValue();
        return "value=" + mv.getSensorValue() ;
    }

    @Override
    public Boolean updateInterface(String str) {
        if (str.equals("false") || str.equals("False") || str.equals("True") ||str.equals("true") ){
            Boolean valueAux = Boolean.parseBoolean(str);
            setValue(new MovementValue(valueAux));
            return false;
        }
        return true;
    }
}
