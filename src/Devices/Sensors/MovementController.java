package Devices.Sensors;

import Devices.DevicesController;
import Values.MovementValue;
import Values.Value;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;

public class MovementController implements DevicesController{
    @FXML
    public ChoiceBox<String> movementValue;

    @FXML
    public void initialize(){
        ObservableList lista = FXCollections.observableArrayList ("True", "False");
        movementValue.setItems(lista);
        movementValue.setValue("True");
    }

    @Override
    public Value getValue() {
        return new MovementValue(Boolean.parseBoolean(movementValue.getValue()));
    }
}
