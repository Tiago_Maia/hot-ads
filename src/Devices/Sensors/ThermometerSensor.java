package Devices.Sensors;

import Devices.DeviceState;
import House.Room;
import Values.MovementValue;
import Values.ThermometerValue;
import Values.Value;

public class ThermometerSensor extends Sensor {

    public ThermometerSensor(Room room, String deviceName, DeviceState deviceState, Value value) {
        super(room, deviceName, deviceState, value);
    }

    public void setValue(ThermometerValue value) {
        checkCondition(value);
        super.setValue(value);
    }

    @Override
    public String getValueString() {
        ThermometerValue tv = (ThermometerValue)getValue();
        if(tv.getNumber() >= tv.getMin() && tv.getNumber() <= tv.getMax()){
            return String.valueOf(tv.getNumber());
        }else{
            return "No value set";
        }
    }


    @Override
    public String toString() {
        ThermometerValue tv = (ThermometerValue)getValue();
        return tv.toString();
    }

    @Override
    public Boolean updateInterface(String str) {
        try {
            ThermometerValue tv = (ThermometerValue)getValue();
            double valueAux = Double.parseDouble(str);
            if (valueAux <= tv.getMax() && valueAux >= tv.getMin()){
                setValue(new ThermometerValue(valueAux, tv.getMin(), tv.getMax()));
                return false;
            }
            return true;
        }catch (Exception e){
            System.out.println("Erro no double");
        }
        return true;
    }

}
