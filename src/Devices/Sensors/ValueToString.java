package Devices.Sensors;

public interface ValueToString {
    String getValueString();
    Boolean updateInterface(String str);
}
