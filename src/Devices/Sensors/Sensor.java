package Devices.Sensors;

import Conditions.Condition;
import Devices.Device;
import Devices.DeviceState;
import House.Room;
import Communication.*;
import Values.Value;

import java.util.ArrayList;

public abstract class Sensor extends Device {
    private ArrayList<Condition> conditions = new ArrayList<>();

    public Sensor(Room room, String deviceName, DeviceState deviceState, Value value) {
        super(room, deviceName, deviceState, value);
    }

    public void checkCondition(Value value){
        for (Condition condition: conditions) {
            System.out.println(condition);
            if(condition.compareCondition(value)){
                execute(condition);
            }
        }
    }

    public void addCondition (Condition condition){
        conditions.add(condition);
    }

    public abstract String getValueString();

    public abstract Boolean updateInterface(String str);

    public void execute(Condition condition)
    {
        Message message = new Message(condition);
        Event.getInstance().publish(message);
    }

    @Override
    public String toString() {
        return "Sensor:" +
                "conditions=" + conditions.toString() +
                '}';
    }

    public ArrayList<Condition> getConditions() {
        return conditions;
    }
}
