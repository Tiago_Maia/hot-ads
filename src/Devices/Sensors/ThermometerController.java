package Devices.Sensors;

import Devices.DevicesController;
import Values.ThermometerValue;
import Values.Value;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;

public class ThermometerController implements DevicesController {
    public TextField maxValue;
    public TextField minValue;
    public TextField value;

    private String maxValueStr, minValueStr, valueStr;

    @FXML
    public void initialize(){
        maxValue.textProperty().addListener((obs, oldText, newText) -> {
            maxValueStr = newText;
        });
        minValue.textProperty().addListener((obs, oldText, newText) -> {
            minValueStr = newText;
        });
        value.textProperty().addListener((obs, oldText, newText) -> {
            valueStr = newText;
        });
    }

    public ThermometerValue getLightValue(){
        Double valueDouble, maxDouble, minDouble;
        try {
            valueDouble = Double.parseDouble(valueStr);
            maxDouble = Double.parseDouble(maxValueStr);
            minDouble = Double.parseDouble(minValueStr);

            if(minDouble>maxDouble || valueDouble<minDouble || valueDouble>maxDouble){
                return null;
            }

            return new ThermometerValue(valueDouble, minDouble, maxDouble);
        }catch (Exception e ){
            return null;
        }
    }

    @Override
    public Value getValue() {
        return getLightValue();
    }
}
