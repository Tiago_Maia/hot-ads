package Devices.Actuators;

import Values.SlackMessageValue;
import Communication.Message;
import Communication.OnMessage;
import Devices.DeviceState;
import House.Room;
import Values.ThermometerValue;
import Values.Value;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.layout.AnchorPane;
import org.json.simple.JSONObject;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class SlackActuator extends Actuator {
    SlackActuatorControler slackActuatorControler;

    public SlackActuator(Room room, String deviceName, DeviceState deviceState, Value value) {
        super(room, deviceName, deviceState, value);
    }

    @OnMessage
    private void onMessage(Message message)
    {
        if (getDeviceState() != DeviceState.OFFLINE){
            super.setValue(message.getCondition().getActuatorAction(this));
            postMessageOnSlack();
        }
    }

    @Override
    public String toString() {
        return getValue().toString();
    }

    @Override
    public void loadInputs(AnchorPane anchorPane) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("../../Resources/Devices/SlackActuator.fxml"));
        Node node;
        node = (Node) loader.load();
        slackActuatorControler = loader.getController();
        anchorPane.getChildren().setAll(node);
    }

    @Override
    public Value getValueCondition() {
        return slackActuatorControler.getMessageValue();
    }

    private void postMessageOnSlack()
    {
        // Create json object to send
        JSONObject jsonObject = new JSONObject();
        SlackMessageValue sk = (SlackMessageValue) getValue();
        jsonObject.put("text", sk.getMessage()); // Pass here the message that we want to send to the slack

        // Create HTTP Client and HTTP Request with the specific Webhook of Slack
        HttpClient httpclient = HttpClient.newBuilder().build();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("https://hooks.slack.com/services/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"))
                .POST(HttpRequest.BodyPublishers.ofString(jsonObject.toJSONString()))
                .build();

        //Get the response back from the slack server , getting "ok" is expected
        HttpResponse<String> response = null;
        try {
            response = httpclient.send(request,
                    HttpResponse.BodyHandlers.ofString());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // Print the response from the slack server
        System.out.println(response.body());
    }
}
