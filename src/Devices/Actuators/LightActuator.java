package Devices.Actuators;

import Communication.Message;
import Communication.OnMessage;
import Devices.DeviceState;
import House.Room;
import Values.*;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;

public class LightActuator extends Actuator {
    LightActuatorControler lightActuatorController;

    public LightActuator(Room room, String deviceName, DeviceState deviceState, Value value) {
        super(room, deviceName, deviceState, value);
    }

    @OnMessage
    private void onMessage(Message message)
    {
        if (getDeviceState() != DeviceState.OFFLINE){
            setValue(message.getCondition().getActuatorAction(this));
        }
    }

    @Override
    public String toString() {
        return "value: " + getValue();
    }

    @Override
    public void loadInputs(AnchorPane anchorPane) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("../../Resources/Devices/LightActuator.fxml"));
        Node node;
        node = (Node) loader.load();
        lightActuatorController = loader.getController();
        anchorPane.getChildren().setAll(node);
    }

    @Override
    public Value getValueCondition() {
        return lightActuatorController.getLightValue();
    }
}
