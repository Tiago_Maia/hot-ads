package Devices.Actuators;

import Values.Value;
import Communication.Event;
import Conditions.Condition;
import Devices.Device;
import Devices.DeviceState;
import House.Room;
import javafx.scene.layout.AnchorPane;


import java.io.IOException;

public abstract class Actuator extends Device
{
    public Actuator(Room room, String deviceName, DeviceState deviceState, Value value) {
        super(room, deviceName, deviceState, value);
    }

    @Override
    public String toString() {
        return super.toString();
    }

    public void Subscribe (Condition condition){
        Event.getInstance().subscribe(condition, this);
    }

    public Value getValueCondition() {
        return null;
    }

    public void loadInputs(AnchorPane anchorPane) throws IOException {
        System.out.println("Erro");
    }
}