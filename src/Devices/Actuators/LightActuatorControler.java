package Devices.Actuators;

import Devices.DevicesController;
import Values.LightValue;
import Values.Value;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;

public class LightActuatorControler implements DevicesController {
    @FXML
    public TextField red;
    public TextField green;
    public TextField blue;
    public TextField intensity;

    private String redValue, greenValue, blueValue, intensityValue;

    @FXML
    public void initialize(){
        red.textProperty().addListener((obs, oldText, newText) -> {
            redValue = newText;
        });
        green.textProperty().addListener((obs, oldText, newText) -> {
            greenValue = newText;
        });
        blue.textProperty().addListener((obs, oldText, newText) -> {
            blueValue = newText;
        });
        intensity.textProperty().addListener((obs, oldText, newText) -> {
            intensityValue = newText;
        });
    }

    public LightValue getLightValue(){
        int redValueInteger, greenValueInteger, blueValueInteger, intensityValueInteger;

        try {
            redValueInteger = Integer.parseInt(redValue);
            greenValueInteger  = Integer.parseInt(greenValue);
            blueValueInteger = Integer.parseInt(blueValue);
            intensityValueInteger = Integer.parseInt(intensityValue);

            return new LightValue(redValueInteger,greenValueInteger, blueValueInteger, intensityValueInteger);
        }catch (Exception e ){
            return null;
        }
    }

    @Override
    public Value getValue() {
        return getLightValue();
    }
}
