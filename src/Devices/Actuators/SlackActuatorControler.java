package Devices.Actuators;

import Devices.DevicesController;
import Values.SlackMessageValue;
import Values.Value;
import javafx.scene.control.TextField;

public class SlackActuatorControler implements DevicesController {
    public TextField textValue;

    public SlackMessageValue getMessageValue(){
        return new SlackMessageValue(textValue.getText());
    }

    @Override
    public Value getValue() {
        return getMessageValue();
    }
}
