package Communication;

import Conditions.Condition;

public class Message
{
    private Condition condition;

    public Message(Condition condition)
    {
        this.condition = condition;
    }


    public Condition getCondition() {
        return condition;
    }
}