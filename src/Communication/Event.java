package Communication;

import Conditions.Condition;
import Devices.Device;
import java.lang.annotation.Annotation;
import java.lang.ref.WeakReference;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class Event
{
    private static final Event INSTANCE = new Event();

    private Event() {}

    public static Event getInstance() {
        return Event.INSTANCE;
    }

    static ConcurrentHashMap<Condition, ConcurrentHashMap<Integer, WeakReference<Device>>> channels = new ConcurrentHashMap<>();

    public void subscribe(Condition condition, Device subscriber)
    {
        if (!channels.containsKey(condition)) {
            channels.put(condition, new ConcurrentHashMap<>());
        }

        channels.get(condition).put(subscriber.hashCode(), new WeakReference<>(subscriber));
    }

    public void publish(Message message)
    {
        if(channels.size() == 0){
            System.out.println("No actuators found");
            return;
        }

        for(Map.Entry<Integer, WeakReference<Device>> subs : channels.get(message.getCondition()).entrySet())
        {
            WeakReference<Device> subscriberRef = subs.getValue();

            Device subscriberObj = subscriberRef.get();

            for (final Method method : subscriberObj.getClass().getDeclaredMethods())
            {
                Annotation annotation = method.getAnnotation(OnMessage.class);
                if (annotation != null)
                {
                    Event.getInstance().deliverMessage(subscriberObj, method, message);
                }
            }
        }
    }

    public boolean deliverMessage(Device subscriber, Method method, Message message)
    {
        try {
            boolean methodFound = false;
            for (final Class paramClass : method.getParameterTypes())
            {
                if (paramClass.equals(message.getClass()))
                {
                    methodFound = true;
                    break;
                }
            }
            if (methodFound)
            {
                method.setAccessible(true);
                method.invoke(subscriber, message);
            }

            return true;
        } catch (Exception e)
        {
            e.printStackTrace();
        }

        return false;
    }
}