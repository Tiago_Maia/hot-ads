package House;

public enum RoomDivision
{
    KITCHEN,
    LIVINGROOM,
    WC,
    BEDROOM,
    GARAGE,
    HALL
}
