package House;

public class Room {
    private String name;
    private int id;
    private RoomDivision roomDivision;
    static int idCounter = 0;

    public Room(String name, RoomDivision typeOfRoom) {
        this.name = name;
        this.id = idCounter;
        this.roomDivision = typeOfRoom;
        idCounter++;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public RoomDivision getRoomDivision() {
        return roomDivision;
    }

    public void setRoomDivision(RoomDivision roomDivision) {
        this.roomDivision = roomDivision;
    }

    public boolean equals (Object obj)
    {
        if (this == obj) return true;

        if (obj == null) return false;

        if (this.getClass() != obj.getClass()) return false;

        Room x = (Room) obj;
        return x.id==this.id;
    }

    @Override
    public String toString() {
        return "RoomName: " + name +
                "\nRoomId: " + id +
                "\nRoomDivision: " + roomDivision;
    }
}
