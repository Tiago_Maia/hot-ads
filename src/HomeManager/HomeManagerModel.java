package HomeManager;

import Devices.Actuators.Actuator;
import Conditions.Condition;
import Devices.Device;
import Devices.Sensors.Sensor;
import House.Room;
import House.RoomDivision;

import java.util.ArrayList;


public class HomeManagerModel
{
    private static ArrayList<Sensor> sensors = new ArrayList<>();
    private static ArrayList<Actuator> actuators = new ArrayList<>();
    private static ArrayList<Room> rooms = new ArrayList<>();
    private static ArrayList<Condition> conditions = new ArrayList<>();

    private static final HomeManagerModel INSTANCE = new HomeManagerModel();

    private HomeManagerModel() {};

    public static HomeManagerModel getInstance() {
        return HomeManagerModel.INSTANCE;
    }

    public static void setRooms() {
        for (RoomDivision roomDivision: RoomDivision.values()){
            Room room = new Room(roomDivision.name(),roomDivision);
            rooms.add(room);
        }
    }

    public void addDevice(Device device)
    {
        if(device instanceof Sensor){
           addSensor((Sensor) device);
        }else{
            addActuator((Actuator) device);
        }
    }

    public void addSensor(Sensor sensor){
        sensors.add(sensor);
    }

    public void addActuator(Actuator actuator){
        actuators.add(actuator);
    }

    public ArrayList<Sensor> getSensors(){
        return sensors;
    }

    public ArrayList<Actuator> getActuators(){
        return actuators;
    }

    public ArrayList<Room> getRooms() {
        return rooms;
    }

    public void setRooms(ArrayList<Room> rooms) {
        this.rooms = rooms;
    }

    public void addRoom(Room room){
        rooms.add(room);
    }

    public ArrayList<Condition> getConditions() {
        return conditions;
    }

    public void addCondition(Condition condition){
        conditions.add(condition);
    }
}
