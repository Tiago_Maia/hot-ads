package HomeManager.Controller;

import Conditions.ConditionType.ConditionControllerInterface;
import Conditions.ConditionsTypes;
import HomeManager.HomeManagerModel;
import Values.Value;
import Conditions.ConditionType.MovementCondition;
import Conditions.ConditionType.ThermometerCondition;
import Devices.Actuators.Actuator;
import Conditions.Condition;
import Devices.Sensors.MovementSensor;
import Devices.Sensors.ThermometerSensor;
import Devices.Sensors.Sensor;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;

public class ConditionController {
    @FXML
    public TextField conditionName;
    public ChoiceBox<String> conditionType;
    public Button addConditionBtn;
    public TableView<Condition> conditionTV;
    public TableColumn<Condition, String> idCondition;
    public TableColumn<Condition, String> nameCondition;
    public TableColumn<Condition, String> condition;
    public ChoiceBox<String> sensorsCB;
    public ChoiceBox<String> actuatorsCB;
    public Button addCondSensorBtn;
    public TextArea conditionActuators;
    public AnchorPane anchorPane;
    public AnchorPane anchorPane1;

    private Condition conditionTemp;
    private Actuator actuatorTemp;
    private ConditionControllerInterface conditionControllerTemp;

    @FXML
    public void initialize() {
        listConditons();
        choiceBoxValueType();
        updateActuatorChoiceBox();
    }

    public String listActuatorsCondition(HashMap<Actuator, Value> hashMap){
        String temp = "";
        for (HashMap.Entry<Actuator, Value> entry : hashMap.entrySet()) {
            Actuator key = entry.getKey();
            Value value = entry.getValue();

            temp= temp + "Actuator ID: "+ key.getId() + "\nAfter condition met\n"+ value.toString() + "\n\n";
        }

        for (Sensor sensor : HomeManagerModel.getInstance().getSensors()) {
            for (Condition condition : sensor.getConditions()) {
                if(condition.equals(conditionTemp)){
                    temp = temp + "Sensor ID with condition: "+ sensor.getId()+"\n";
                }
            }
        }

        return temp;
    }

    public void listConditons(){
        ObservableList<Condition> items = FXCollections.observableArrayList ();

        for (Condition condition : HomeManagerModel.getInstance().getConditions()) {
            items.add(condition);
        }

        conditionTV.setItems(items);
        conditionTV.setEditable(true);
        idCondition.setCellValueFactory((TableColumn.CellDataFeatures<Condition, String> p) -> new ReadOnlyStringWrapper(String.valueOf(p.getValue().getId())));
        nameCondition.setCellValueFactory((TableColumn.CellDataFeatures<Condition, String> p) -> new ReadOnlyStringWrapper(p.getValue().getName()));
        condition.setCellValueFactory((TableColumn.CellDataFeatures<Condition, String> p) -> new ReadOnlyStringWrapper((p.getValue().toString())));
        conditionTV.setOnMouseClicked((MouseEvent event) -> {
            if(event.getButton().equals(MouseButton.PRIMARY)){
                Condition conditionClicked = conditionTV.getSelectionModel().getSelectedItem();
                conditionTemp = conditionClicked;
                conditionActuators.setText(listActuatorsCondition(conditionClicked.getActuatorAction()));
                updateSensorChoiceBox(conditionTemp);
            }
        });
    }

    public void updateSensorChoiceBox(Condition condition){
        ObservableList<String> sensors = FXCollections.observableArrayList();

        for(Sensor sensor: HomeManagerModel.getInstance().getSensors()){
            if(condition instanceof MovementCondition && sensor instanceof MovementSensor){
                sensors.add(String.valueOf(sensor.getId()));
            }else if(condition instanceof ThermometerCondition && sensor instanceof ThermometerSensor){
                sensors.add(String.valueOf(sensor.getId()));
            }
        }
        sensorsCB.setItems(sensors);
        sensorsCB.setValue("ID");
    }

    public void updateActuatorChoiceBox(){
        ObservableList<String> actuators = FXCollections.observableArrayList();

        for(Actuator actuator: HomeManagerModel.getInstance().getActuators()){
            actuators.add(String.valueOf(actuator.getId()));
        }
        actuatorsCB.setItems(actuators);
        actuatorsCB.setValue("ID");

        actuatorsCB.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number number, Number number2) {

                for(Actuator actuator: HomeManagerModel.getInstance().getActuators()){

                    if(actuator.getId() == Integer.parseInt(actuatorsCB.getItems().get((Integer) number2))){
                        actuatorTemp = actuator;
                        try {
                            actuatorTemp.loadInputs(anchorPane);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });
    }

    public void choiceBoxValueType(){
        ObservableList lista = FXCollections.observableArrayList ();

        for (ConditionsTypes conditionsTypes: ConditionsTypes.values()) {
            lista.add(conditionsTypes.toString());
        }

        conditionType.setItems(lista);
        conditionType.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number number, Number number2) {
                try {
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("../../Resources/ConditionResources/"+conditionType.getItems().get((Integer) number2)+".fxml"));
                    Node node;
                    node = (Node) loader.load();
                    conditionControllerTemp = loader.getController();
                    anchorPane1.getChildren().setAll(node);
                } catch (IOException e) {
                    System.out.println(e);
                }
            }
        });
    }

    public void addConditionToSensor(){
        String sensorIdChosen = sensorsCB.getValue();
        for(Sensor sensor: HomeManagerModel.getInstance().getSensors()){
            if (sensor.getId() == Integer.parseInt(sensorIdChosen)) {
                sensor.addCondition(conditionTemp);
                System.out.println(sensor.getConditions().toString());
            }
        }
    }

    public void addActuatorToCondition() {
        if (actuatorTemp != null){
            Value valueTemp = actuatorTemp.getValueCondition();
            if (valueTemp != null){
                actuatorTemp.Subscribe(conditionTemp);
                conditionTemp.addDevice(actuatorTemp, valueTemp);
            }
        }
    }

    public void addConditionBtnClicked() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException, ClassNotFoundException {
        String conditionNameValue = conditionName.getText();

        if (conditionControllerTemp == null || conditionType.getValue() == null){
            System.out.println("Error");
            return;
        }

        if (conditionControllerTemp.getValue() != null && !conditionNameValue.equals("")){
            System.out.println("Conditions.ConditionType."+ conditionType.getValue());
            Class<?> c = Class.forName("Conditions.ConditionType."+ conditionType.getValue());
            Constructor<?> cons = c.getConstructor(String.class, String.class, Value.class);
            Condition condition = (Condition) cons.newInstance(conditionControllerTemp.getOperator(), conditionNameValue, conditionControllerTemp.getValue());

            HomeManagerModel.getInstance().addCondition(condition);
            listConditons();
        }
    }

}
