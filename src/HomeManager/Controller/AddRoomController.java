package HomeManager.Controller;

import HomeManager.HomeManagerModel;
import House.Room;
import House.RoomDivision;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;

public class AddRoomController
{
    @FXML
    public ChoiceBox<String> roomType;
    public TextField roomName;

    @FXML
    public void initialize()
    {
        addRoomType();
    }

    public void addRoomCliked(ActionEvent actionEvent)
    {
        String roomTypeValue = roomType.getValue();
        String roomNameValue = roomName.getText();
        RoomDivision newRoomDivision = null;

        for (RoomDivision roomDivision : RoomDivision.values()){
            if (roomDivision.name().equals(roomTypeValue)){
                newRoomDivision = roomDivision;
                break;
            }
        }

        if (newRoomDivision == null){
            System.out.println("Error, no room division");
            return;
        }

        if (roomNameValue.equals("")){
            System.out.println("Error, no room name");
            return;
        }

        Room newRoom = new Room(roomNameValue, newRoomDivision);
        HomeManagerModel.getInstance().addRoom(newRoom);
    }

    public void addRoomType(){
        ObservableList lista = FXCollections.observableArrayList ();

        for (RoomDivision roomDivision : RoomDivision.values()){
            lista.add(roomDivision.name());
        }

        roomType.setItems(lista);
        roomType.setValue("KITCHEN");
    }
}
