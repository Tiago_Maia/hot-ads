package HomeManager.Controller;
import Devices.*;
import Devices.Actuators.Actuator;
import Devices.Sensors.Sensor;
import HomeManager.HomeManagerModel;
import House.Room;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.ComboBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Circle;

public class HomeController
{
    @FXML
    public ChoiceBox<String> roomchoice;
    public TableView<Sensor> sensorsttv;
    public TableColumn<Sensor, String> idColumnSensor;
    public TableColumn<Sensor, String> nameColumnSensor;
    public TableColumn<Sensor, String> booleanColumnSensor;
    public TableColumn<Sensor, String> roomDivisionColumnSensor;
    public TableColumn<Sensor, String> valueColumnSensor;
    public TableColumn<Sensor, Circle> stateColumnSensor;
    public TableColumn<Sensor, String> onOffColumnSensor;
    public TableView<Actuator> atuatorsttv1;
    public TableColumn<Actuator, String> idColumnActuator;
    public TableColumn<Actuator, String> nameColumnActuator;
    public TableColumn<Actuator, String> roomDivisionColumnActuator;
    public TableColumn<Actuator, Circle> stateColumnActuator;
    public TableColumn<Actuator, String> onOffColumnActuator;
    public TextArea detailsSensor;
    public TextArea detailsActuator;

    @FXML
    public void initialize() {
        updateChoiceRoom();
    }

    public void updateChoiceRoom(){
        ObservableList<String> rooms = FXCollections.observableArrayList();

        for(Room room: HomeManagerModel.getInstance().getRooms()){
            rooms.add(String.valueOf(room.getName()));
        }

        boolean tempRoomChoiceBool = false;
        String tempRoomChoice = null;
        if (roomchoice.getValue() != null){
            tempRoomChoice = roomchoice.getValue();
            tempRoomChoiceBool = true;
        }

        roomchoice.setItems(rooms);
        roomchoice.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number number, Number t1) {
                //Nota chama a função quando é adicionado aos rooms, pelo que tem que ser verificado se e maior que -1
                if((Integer)observableValue.getValue()>=0){
                    String roomChoosen = rooms.get(Integer.parseInt(String.valueOf(observableValue.getValue())));
                    updateSensorsTable(roomChoosen);
                    updateActuatorsTable(roomChoosen);
                }
            }
        });

        if (rooms.size() > 0){
            if (!tempRoomChoiceBool){
                roomchoice.setValue(rooms.get(0));
            }else{
                roomchoice.setValue(tempRoomChoice);
            }
        }
    }

    public void updateActuatorsTable(String roomChoosen) {
        ObservableList<Actuator> items = FXCollections.observableArrayList();

        for (Actuator actuator : HomeManagerModel.getInstance().getActuators()) {
            if (roomChoosen.equals(String.valueOf(actuator.getRoom().getName()))) {
                items.add(actuator);
            }
        }

        atuatorsttv1.setItems(items);
        atuatorsttv1.setEditable(true);
        idColumnActuator.setCellValueFactory((TableColumn.CellDataFeatures<Actuator, String> p) -> new ReadOnlyStringWrapper(String.valueOf(p.getValue().getId())));
        nameColumnActuator.setCellValueFactory((TableColumn.CellDataFeatures<Actuator, String> p) -> new ReadOnlyStringWrapper(String.valueOf(p.getValue().getDeviceName())));
        roomDivisionColumnActuator.setCellValueFactory((TableColumn.CellDataFeatures<Actuator, String> p) -> new ReadOnlyStringWrapper(String.valueOf(p.getValue().getRoom().getName())));
        stateColumnActuator.setCellValueFactory((TableColumn.CellDataFeatures<Actuator, Circle> p) -> new SimpleObjectProperty(p.getValue().getCircle()));
        stateColumnActuator.setStyle("-fx-alignment: CENTER;");
        onOffColumnActuator.setCellValueFactory(new PropertyValueFactory<>("deviceState"));
        onOffColumnActuator.setCellFactory(ComboBoxTableCell.forTableColumn("OFFLINE", "ONLINE"));
        atuatorsttv1.setOnMouseClicked((MouseEvent event) -> {
            if(event.getButton().equals(MouseButton.PRIMARY)){
                Actuator actuatorClicked = atuatorsttv1.getSelectionModel().getSelectedItem();
                if(actuatorClicked != null){
                    detailsActuator.setText(actuatorClicked.toString());
                }
            }
        });
    }

    public void updateSensorsTable(String roomChoosen){
        ObservableList<Sensor> items = FXCollections.observableArrayList ();

        for (Sensor sensor : HomeManagerModel.getInstance().getSensors()) {
            if(roomChoosen.equals(String.valueOf(sensor.getRoom().getName()))) {
                items.add(sensor);
            }
        }

        sensorsttv.setItems(items);
        sensorsttv.setEditable(true);
        idColumnSensor.setCellValueFactory((TableColumn.CellDataFeatures<Sensor, String> p) -> new ReadOnlyStringWrapper(String.valueOf(p.getValue().getId())));
        nameColumnSensor.setCellValueFactory((TableColumn.CellDataFeatures<Sensor, String> p) -> new ReadOnlyStringWrapper(String.valueOf(p.getValue().getDeviceName())));
        booleanColumnSensor.setCellValueFactory((TableColumn.CellDataFeatures<Sensor, String> p) -> new ReadOnlyStringWrapper(p.getValue().getClass().getName().replace("Devices.Sensors.","")));
        roomDivisionColumnSensor.setCellValueFactory((TableColumn.CellDataFeatures<Sensor, String> p) -> new ReadOnlyStringWrapper(String.valueOf(p.getValue().getRoom().getName())));
        stateColumnSensor.setCellValueFactory((TableColumn.CellDataFeatures<Sensor, Circle> p) -> new SimpleObjectProperty(p.getValue().getCircle()));
        stateColumnSensor.setStyle("-fx-alignment: CENTER;");
        valueColumnSensor.setCellValueFactory((TableColumn.CellDataFeatures<Sensor, String> p) -> new ReadOnlyStringWrapper(p.getValue().getValueString()));
        valueColumnSensor.setCellFactory(TextFieldTableCell.forTableColumn());
        onOffColumnSensor.setCellValueFactory(new PropertyValueFactory<>("deviceState"));
        onOffColumnSensor.setCellFactory(ComboBoxTableCell.forTableColumn("OFFLINE", "ONLINE"));
        sensorsttv.setOnMouseClicked((MouseEvent event) -> {
            if(event.getButton().equals(MouseButton.PRIMARY)){
                Sensor sensorClicked = sensorsttv.getSelectionModel().getSelectedItem();
                if (sensorClicked != null){
                    detailsSensor.setText(sensorClicked.toString());
                }
            }
        });
    }

    public void onEditValue(TableColumn.CellEditEvent<Sensor, String> sensorTempStringCellEditEvent) {
        Sensor sensorTemp = sensorsttv.getSelectionModel().getSelectedItem();

        for(Sensor sensor: HomeManagerModel.getInstance().getSensors()){
            if(sensor.getId() == sensorTemp.getId()){
                if (sensor.updateInterface(sensorTempStringCellEditEvent.getNewValue())){
                    updateSensorsTable(roomchoice.getValue());
                }
            }
        }
    }

    public void onEditState(TableColumn.CellEditEvent<Actuator, String> actuatorStringCellEditEvent) {

        Actuator actuatorTemp = atuatorsttv1.getSelectionModel().getSelectedItem();
        String valueChosen = actuatorStringCellEditEvent.getNewValue();

        for(Actuator actuator: HomeManagerModel.getInstance().getActuators()){
            if(actuator.getId() == actuatorTemp.getId()){

                if(valueChosen.equals("OFFLINE")){
                    actuator.setDeviceState(DeviceState.OFFLINE);
                }else if(valueChosen.equals("ONLINE")){
                    actuator.setDeviceState(DeviceState.ONLINE);
                }
            }
        }
    }

    public void onEditStateSensor(TableColumn.CellEditEvent<Sensor, String> sensorStringCellEditEvent) {
        Sensor sensorTemp = sensorsttv.getSelectionModel().getSelectedItem();
        String valueChosen = sensorStringCellEditEvent.getNewValue();

        for(Sensor sensor: HomeManagerModel.getInstance().getSensors()){
            if(sensor.getId() == sensorTemp.getId()){
                if(valueChosen.equals("OFFLINE")){
                    sensor.setDeviceState(DeviceState.OFFLINE);
                }else if(valueChosen.equals("ONLINE")){
                    sensor.setDeviceState(DeviceState.ONLINE);
                }

            }
        }
    }
}
