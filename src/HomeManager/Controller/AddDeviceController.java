package HomeManager.Controller;

import HomeManager.HomeManagerModel;
import House.Room;
import Devices.Actuators.ActuatorType;
import Devices.Device;
import Devices.DeviceState;
import Devices.DevicesController;
import Devices.Sensors.SensorTypes;
import Values.Value;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class AddDeviceController
{
    @FXML
    public ChoiceBox<String> deviceType;
    public TextField deviceName;
    public ChoiceBox<String> roomchoice1;
    public AnchorPane anchorPane1;
    public ChoiceBox<String> enumsCB;

    private DevicesController devicesController;

    @FXML
    public void initialize()
    {
        ObservableList lista = FXCollections.observableArrayList ("Actuators","Sensors");
        deviceType.setItems(lista);
        choiceBoxDeviceType();
        addRoomChoices();
    }

    public void choiceBoxDeviceType()
    {
        deviceType.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number number, Number number2) {

                ObservableList lista = FXCollections.observableArrayList();
                if(deviceType.getItems().get((Integer) number2).equals("Sensors")){
                    for (SensorTypes sensorTypes : SensorTypes.values()) {
                        lista.add(sensorTypes.toString());
                    }
                }else{
                    for (ActuatorType actuatorTypes : ActuatorType.values()) {
                        lista.add(actuatorTypes.toString());
                    }
                }
                System.out.println(anchorPane1.getChildren().size());
                if(anchorPane1.getChildren().size() > 0){
                    try{
                        for (Node a: anchorPane1.getChildren()) {
                            anchorPane1.getChildren().removeAll(a);
                        }
                    }catch(Exception e){
                        System.out.println("Erro " + e.getMessage());
                    }
                    enumsCB.getSelectionModel().clearSelection();
                    enumsCB.getItems().clear();
                    System.out.println("ADSsadsda");
                    enumsCB.setItems(lista);
                    return;
                }

                enumsCB.setItems(lista);
                enumsCB.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
                    @Override
                    public void changed(ObservableValue<? extends Number> observableValue, Number number, Number number2) {
                        try {
                            FXMLLoader loader = new FXMLLoader(getClass().getResource("../../Resources/Devices/"+enumsCB.getItems().get((Integer) number2)+".fxml"));
                            Node node;
                            node = (Node) loader.load();
                            devicesController = loader.getController();
                            anchorPane1.getChildren().setAll(node);
                        } catch (IOException e) {
                            System.out.println(e);
                        }
                    }
                });
            }
        });
    }

    public void addRoomChoices()
    {
        ObservableList<String> rooms = FXCollections.observableArrayList();

        for(Room room: HomeManagerModel.getInstance().getRooms()){
            rooms.add(String.valueOf(room.getName()));
        }
        roomchoice1.setItems(rooms);
    }

    public void addDeviceBtnCliked(ActionEvent actionEvent) throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        String selectedDeviceName = deviceName.getText();
        String selectedRoom = roomchoice1.getSelectionModel().getSelectedItem().toString();
        Room roomTemp = null;

        for(Room room: HomeManagerModel.getInstance().getRooms()){
            if(room.getName().equals(selectedRoom)){
                roomTemp = room;
            }
        }

        if (devicesController == null || enumsCB.getValue() == null){
            return;
        }

        if (devicesController.getValue() != null && !selectedDeviceName.equals("")){
            Device device;
            Class<?> c = Class.forName("Devices." + deviceType.getValue() + "." + enumsCB.getValue());
            Constructor<?> cons = c.getConstructor(Room.class, String.class, DeviceState.class, Value.class);
            device = (Device) cons.newInstance(roomTemp, selectedDeviceName, DeviceState.ONLINE, devicesController.getValue());
            HomeManagerModel.getInstance().addDevice(device);
        }
    }
}
