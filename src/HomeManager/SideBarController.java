package HomeManager;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.layout.BorderPane;
import java.io.IOException;


public class SideBarController
{
    @FXML
    public BorderPane bp;

    public void Home() { loadPage("Home");}

    public void Home(javafx.scene.input.MouseEvent mouseEvent) { loadPage("Home");}

    public void AddDevice(javafx.scene.input.MouseEvent mouseEvent) {
        loadPage("AddDevice");
    }

    public void AddRoom(javafx.scene.input.MouseEvent mouseEvent) {
        loadPage("AddRoom");
    }

    public void Condition(javafx.scene.input.MouseEvent mouseEvent) {
        loadPage("Condition");
    }

    private void loadPage(String page){
        Parent root = null;

        try {
            root = FXMLLoader.load(getClass().getResource("../Resources/" +page+".fxml"));
        }catch (IOException ex){
            System.out.print("Did not load the new page" + ex);
        }

        bp.setCenter(root);
    }
}
