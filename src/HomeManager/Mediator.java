package HomeManager;

import Devices.Actuators.Actuator;
import Conditions.Condition;
import Devices.Device;
import Devices.Sensors.Sensor;
import House.*;

import java.util.ArrayList;

/*public class Mediator {
    private HomeManagerModel model;
    private static final Mediator INSTANCE = new Mediator();

    private Mediator() {};

    public static Mediator getInstance() {
        return Mediator.INSTANCE;
    }

    public void setup(HomeManagerModel homeManagerModel)
    {
        this.model = homeManagerModel;
    }

    public ArrayList<Room> getRooms()
    {
        return model.getRooms();
    }

    public ArrayList<Actuator> getActuators()
    {
        return  model.getActuators();
    }

    public ArrayList<Condition> getConditions()
    {
        return  model.getConditions();
    }

    public ArrayList<Sensor> getSensors()
    {
        return  model.getSensors();
    }

    public void addDevice(Device device)
    {
        if(device instanceof Sensor){
            model.addSensor((Sensor) device);
        }else{
            model.addActuator((Actuator) device);
        }
    }

    public void addRoom(Room newRoom)
    {
        model.addRoom(newRoom);
    }

    public void addCondition(Condition condition){
        model.addCondition(condition);
    }
}
*/