import Devices.*;
import Devices.Actuators.Actuator;
import Devices.Actuators.LightActuator;
import Devices.Sensors.ThermometerSensor;
import Devices.Sensors.Sensor;
import HomeManager.*;
import HomeManager.SideBarController;
import House.Room;
import House.RoomDivision;
import Values.ThermometerValue;
import Values.LightValue;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import java.util.ArrayList;


public class Main extends Application
{

    @Override
    public void start(Stage primaryStage) throws Exception{
        FXMLLoader loader = new FXMLLoader(getClass().getResource("Resources/SideBar.fxml"));
        Parent root = loader.load();

        HomeManagerModel.setRooms();

        // CONTROLLERS CREATION
        SideBarController sideBarController = loader.getController();

        // Load Home Page
        sideBarController.Home();

        primaryStage.setTitle("House of Things");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

    //Creates the homeManagerModel with "sensorsNumber" of sensors and "actuatorsNumber" of actuators
    /*public void createHomeManager(int roomNumber, int deviceNumber){
        ArrayList<Room> rooms = createRooms(roomNumber);
        HomeManagerModel.setRooms(rooms);

        /*ArrayList<Sensor> sensors = createSensors(deviceNumber,rooms);

        for (Sensor sensor: sensors) {
            homeManagerModel.addSensor(sensor);
        }

        ArrayList<Actuator> actuators = createActuator(deviceNumber, rooms);

        for (Actuator actuator: actuators) {
            homeManagerModel.addActuator(actuator);
        }
    }

    public static ArrayList<Room> createRooms(int roomNumber){
        ArrayList<Room> rooms = new ArrayList<>();

        for (RoomDivision roomDivision: RoomDivision.values()){
            for (int e= 0; e< roomNumber; e++){
                Room room = new Room(roomDivision.name()+e,roomDivision);
                rooms.add(room);
            }
        }

        return rooms;
    }

    //Creates "number" of sensors
    /*public static ArrayList<Sensor> createSensors(int number, ArrayList<Room> rooms){
        ArrayList<Sensor> sensors = new ArrayList<>();

        for (Room room: rooms) {
            for (int i=0; i<number; i++) {
                Sensor sensor = new ThermometerSensor(room, "sensor"+room.getName() +i, DeviceState.ONLINE, new ThermometerValue(-20, -20, 20));
                sensors.add(sensor);
            }
        }

        return sensors;
    }


    //Creates "number" of actuators
    public static ArrayList<Actuator> createActuator(int number, ArrayList<Room> rooms){
        ArrayList<Actuator> actuators = new ArrayList<>();

        for (Room room: rooms) {
            for (int i = 0; i < number; i++) {
                Actuator actuator = new LightActuator(room, "actuator"+room.getName() + i, DeviceState.ONLINE, new LightValue(5,5,5,5));
                actuators.add(actuator);
            }
        }
        return actuators;
    }*/
}

