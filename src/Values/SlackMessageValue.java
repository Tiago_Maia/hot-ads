package Values;

public class SlackMessageValue extends Value {
    private String message;

    public SlackMessageValue(String message) {
        this.message = message;
    }

    @Override
    public SlackMessageValue getValue() {
        return this;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return "message='" + message + "' ";
    }
}
