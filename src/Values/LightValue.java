package Values;

public class LightValue extends Value {
    private int red, green, blue, intensity;

    public LightValue(int red, int green, int blue, int intensity) {
        this.red = red;
        this.green = green;
        this.blue = blue;
        this.intensity = intensity;
    }

    @Override
    public LightValue getValue() {
        return this;
    }

    @Override
    public String toString() {
        return "\n{\nred: " + red +
                "\ngreen: " + green +
                "\nblue: " + blue +
                "\nintensity: " + intensity + "\n}";
    }
}
