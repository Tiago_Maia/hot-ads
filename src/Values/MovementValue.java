package Values;

public class MovementValue extends Value {
    private Boolean value;

    public MovementValue(Boolean value) {
        this.value = value;
    }

    public Boolean getSensorValue() {
        return value;
    }

    @Override
    public MovementValue getValue(){
        return this;
    }

    @Override
    public String toString() {
        return "BoleanValue{" +
                "value=" + value +
                '}';
    }
}
