package Values;

public class ThermometerValue extends Value {
    private double number;
    private double min;
    private double max;

    public ThermometerValue(double number, double min, double max) {
        this.number = number;
        this.min = min;
        this.max = max;
    }

    public ThermometerValue(double number) {
        this.number = number;
    }

    public double getNumber() {
        return number;
    }

    public double getMin() {
        return min;
    }

    public double getMax() {
        return max;
    }

    @Override
    public ThermometerValue getValue(){
        return this;
    }

    @Override
    public String toString() {
        return "{" +
                "number=" + number +
                ", min=" + min +
                ", max=" + max +
                '}';
    }
}
