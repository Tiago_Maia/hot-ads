package Conditions;

import Values.Value;

public interface CompareConditions {
    boolean compareCondition(Value newValue);
}
