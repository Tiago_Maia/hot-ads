package Conditions;

import java.util.HashMap;

import Devices.Actuators.Actuator;
import Values.Value;

public abstract class Condition {
    private static int number = 0;
    private final int id;
    private String name;
    private String operator;
    private HashMap<Actuator, Value> actuatorAction = new HashMap<>();

    public Condition(String operator, String name) {
        this.operator = operator;
        this.name = name;
        this.id = number;
        number++;
    }

    public HashMap<Actuator, Value> getActuatorAction() {
        return actuatorAction;
    }

    public int getId() {
        return id;
    }

    public String getOperator() {
        return operator;
    }

    public Value getActuatorAction(Actuator actuator) {
        return actuatorAction.get(actuator).getValue();
    }

    public void addDevice(Actuator actuator, Value value){
        actuatorAction.put(actuator, value);
    }

    public String getName(){
        return name;
    }

    public abstract boolean compareCondition(Value newValue);

    public String toString (){
        return "Condition: " + name + " with operator " + operator;
    }
}
