package Conditions.ConditionType;

import Values.MovementValue;
import Values.Value;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;

public class MovementConditionController implements ConditionControllerInterface {
    @FXML
    public ChoiceBox <String> operator;
    public ChoiceBox <String> value;

    @FXML
    public void initialize() {
        ObservableList lista = FXCollections.observableArrayList ("==", "!=");
        operator.setItems(lista);
        operator.setValue("==");

        lista = FXCollections.observableArrayList ("True", "False");
        value.setItems(lista);
        value.setValue("True");
    }

    @Override
    public Value getValue() {
        return new MovementValue(Boolean.parseBoolean(value.getValue()));
    }

    @Override
    public String getOperator() {
        return operator.getValue();
    }
}
