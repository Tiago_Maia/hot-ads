package Conditions.ConditionType;

import Conditions.Condition;
import Values.MovementValue;
import Values.Value;

public class MovementCondition extends Condition {
    private MovementValue value;

    public MovementCondition(String operator, String name, Value value) {
        super(operator, name);
        this.value = (MovementValue) value;
    }

    @Override
    public boolean compareCondition(Value newValue) {
        if (getOperator() == "=="){
            return ((MovementValue) newValue).getSensorValue() == value.getSensorValue();
        }else{
            return ((MovementValue) newValue).getSensorValue() != value.getSensorValue();
        }
    }

    @Override
    public String toString() {
        return "BooleanCondition{" +
                "value=" + value.getSensorValue() + " operator "+getOperator()+
                '}';
    }
}
