package Conditions.ConditionType;

import Conditions.Condition;
import Values.ThermometerValue;
import Values.Value;

public class ThermometerCondition extends Condition {
    private ThermometerValue valueCondition;

    public ThermometerCondition(String operator, String name, Value value) {
        super(operator, name);
        this.valueCondition = (ThermometerValue)value;
    }

    @Override
    public boolean compareCondition(Value newValue) {
        switch (getOperator()) {
            case ">":
                if (((ThermometerValue) newValue).getNumber() > valueCondition.getNumber()) {
                    return true;
                }
                break;
            case "<":
                if (((ThermometerValue) newValue).getNumber() < valueCondition.getNumber()) {
                    return true;
                }
                break;
            case "==":
                if (valueCondition.getNumber() == ((ThermometerValue) newValue).getNumber()) {
                    return true;
                }
                break;
            case ">=":
                if ( ((ThermometerValue) newValue).getNumber() >= valueCondition.getNumber()){
                    return true;
                }
                break;
            case "<=":
                if (((ThermometerValue) newValue).getNumber() <= valueCondition.getNumber()){
                    return true;
                }
                break;
            case "!=":
                if (valueCondition.getNumber() !=  ((ThermometerValue) newValue).getNumber()){
                    return true;
                }
                break;
            default:
                System.out.println("error");
        }

        return false;
    }

    @Override
    public String toString() {
        return "DoubleCondition{" +
                "value=" + valueCondition.getNumber() + " operator "+getOperator()+
                '}';
    }

}
