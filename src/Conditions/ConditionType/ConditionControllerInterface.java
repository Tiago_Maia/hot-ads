package Conditions.ConditionType;

import Values.Value;

public interface ConditionControllerInterface {
    Value getValue();
    String getOperator();
}
