package Conditions.ConditionType;

import Values.ThermometerValue;
import Values.Value;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;

public class ThermometerConditionController implements ConditionControllerInterface {

    public ChoiceBox <String>  operator;
    public TextField value;

    @FXML
    public void initialize() {
        ObservableList lista = FXCollections.observableArrayList ("==", "!=", ">", "<", ">=", "<=");
        operator.setItems(lista);
        operator.setValue("==");
    }

    @Override
    public Value getValue() {
        try {
            double aux = Double.parseDouble(value.getText());
            return new ThermometerValue(aux);
        }catch (Exception e){
            System.out.println(e);
        }
        return null;
    }

    @Override
    public String getOperator() {
        return operator.getValue();
    }
}
